# Introduction: 

This repository provides access to the R script connected to the following manuscript:

Pallandt M., Ahrens B., Koirala S., Lange H., Reichstein M., Schrumpf M., Zaehle S: "Vertically divergent responses of SOC decomposition to soil moisture in a changing climate."

The manuscript contains an application of the DAMM model by Davidson et al. (2012) with soil moisture and soil temperature data from the CMIP5 ensemble (https://esgf-node.llnl.gov/projects/esgf-llnl/) and soil data from SoilGrids (https://soilgrids.org/).

# Developer:
The script was written by [Marleen Pallandt](https://www.bgc-jena.mpg.de/bgi/index.php/People/MarleenPallandt) and [Bernhard Ahrens](https://www.bgc-jena.mpg.de/bgi/index.php/People/BernhardAhrens) at the [Department of Biogeochemical Integration, Max Planck Institute for Biogeochemistry](https://www.bgc-jena.mpg.de/bgi/index.php/Main/HomePage) in Jena, Germany. All questions and information shall be directed to [mpalla[at]bgc-jena.mpg.de](mpalla@bgc-jena.mpg.de).

# Usage:

For the scripts to work, the corresponding data of the CMIP5 database and SoilGrids databases need to be downloaded and processed according to the methods section of the manuscript. We include example input files (.RData objects) to run the script. The paths to the data files should be changed by the user.

The climate data are freely available the [CMIP5 data portal](https://esgf-node.llnl.gov/projects/esgf-llnl/) (registration required) and [SoilGrids website](https://soilgrids.org/).

# Requires:

R or RStudio. This script was written under version 3.6.1.

# Disclaimer:

This repository is created to support the manuscript mentioned above. Any usage beyond the intended purpose is the responsibility of the users.